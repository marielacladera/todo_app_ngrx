import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit,  } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import * as actions from 'src/app/filter/filter.actions';
import { deleteAllComplete } from '../todo.actions';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Todo } from '../models/todo.model';

@Component({
  selector: 'app-todo-footer',
  templateUrl: './todo-footer.component.html',
  styleUrls: ['./todo-footer.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoFooterComponent implements OnInit, OnDestroy {

  public filters: actions.validsFilters[];
  public pending: number;

  public actualFilter$?: Observable<actions.validsFilters>;

  private _unSubscribeSubject: Subject<void>;

  constructor(
    private _store: Store<AppState>,
    private _cdr: ChangeDetectorRef
  ) {
    this._unSubscribeSubject = new Subject<void>();
    this.filters = ['todos', 'complete', 'pending'];
    this.pending = 0
  }

  public ngOnInit(): void {
    this._initialize()
  }

  public ngOnDestroy(): void {
      this._finalize();
  }

  public changeFilter(filter: actions.validsFilters) {
    this._store.dispatch(actions.setFilter({filter: filter}));
  }

  public clearComplete(): void{
    this._store.dispatch(deleteAllComplete());
  }

  private _initialize() {
    this._countPendingTodo();
    this._loadActualFilter();
  }

  private _loadActualFilter(): void {
    this.actualFilter$ = this._store.select('filter');
  }

  private _countPendingTodo(): void {
    this._store.select('todos').pipe(takeUntil(this._unSubscribeSubject)).subscribe((state: Todo[]) => {
      this.pending = state.filter(todo => !todo.completed).length;
      this._cdr.markForCheck();
    });
  }

  private _finalize(): void {
    this._unSubscribeSubject.next();
    this._unSubscribeSubject.complete();
  }

}
